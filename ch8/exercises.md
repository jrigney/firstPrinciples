# Chapter 8 Non-code Exercises
## Intermission Exercise
```
applyTimes 5 (+1) 5

(+1) . applyTimes 4 (+1) 5
(+1) . (+1) . applyTimes 3 (+1) 5
(+1) . (+1) . (+1) . applyTimes 2 (+1) 5
(+1) . (+1) . (+1) . (+1) . applyTimes 1 (+1) 5
(+1) . (+1) . (+1) . (+1) . (+1) . applyTimes 0 (+1) 5
(+1) . (+1) . (+1) . (+1) . (+1) $ 5
```

(.) :: (b -> c) -> (a -> b) -> a -> c
(+1) :: Num x => x -> x
(+1) :: Num y => y -> y

Let's label the two (+1) as x and y to avoid confusion
(+1) . (+1)
  x  .   y

(.) :: (y -> y) -> (a -> y) -> a -> y --sub in y version of (+1)
       (b -> c) -> (a -> b) -> a -> c
       (y -> y) -> (x -> y) -> x -> y --sub in x version of (+1)
                         x          x --the y here is the same as the x because x is what is the result of (+1) the x version but leave it as y so you can see how the result moves from the first function (+1)-x-version throug the (+1)-y-version.

so the result would be x -> y where x is the parameter to (+1)-x-version and y is the result of (+1)-y-version


