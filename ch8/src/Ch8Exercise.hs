module Ch8Exercise
  (
  ) where

-- Review types
-- 1 d
-- 2 b
-- 3 d
-- 4 b
-- Review currying
--1 woops mrow woohoo!
--2 1 mrow haha
--3 woops mrow 2 mrow haha
--4 woops mrow blue mrow haha
--5 pink mrow haha mrow green  mrow woops mrow blue
--6 are mrow Pugs mrow awesome
cattyConny :: String -> String -> String
cattyConny x y = x ++ " mrow " ++ y

flippy :: String -> String -> String
flippy = flip cattyConny

appedCatty :: String -> String
appedCatty = cattyConny "woops"

frappe :: String -> String
frappe = flippy "haha"

-- Recursion
-- 1
{-|
   dividedBy 15 2
   go 13 2 1
   go 11 2 2
   go 9 2 3
   go 7 2 4
   go 5 2 5
   go 3 2 6
   go 1 2 7
   (6,1)
-}
--2
mysum :: (Eq a, Num a) => a -> a
mysum x = go x 0
  where
    go n acc
      | n == 0 = acc
      | otherwise = go (n - 1) (acc + n)

--3
myprod :: (Integral a) => a -> a -> a
myprod x y = go x y 0
  where
    go x y acc
      | x == 0 = acc
      | otherwise = go (x - 1) y acc + y

-- Fixing dividedBy
dividedBy :: Integral a => a -> a -> DividedResult
dividedBy num denom
  | denom == 0 = DividedByZero
  | otherwise = Result $ go num 0
  where
    go n count
      | n < denom = count
      | otherwise = go (n - denom) (count + 1)

data DividedResult
  = Result Integer
  | DividedByZero
  deriving (Eq, Show)

-- McCarthy 91 function
mc91 :: (Integral a) => a -> a
mc91 x
  | x > 100 = x - 10
  | x <= 100 = mc91 . mc91 $ x + 11
