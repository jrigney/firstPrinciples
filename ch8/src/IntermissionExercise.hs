module IntermissionExercise
  (
  ) where

-- why is the $ b needed why not just do applyTimes (n-1) f b?
-- because the function composition need applyTimes to be a function
-- so the $ b leave a parameter off the applyTimes making it a function.
applyTimes :: (Eq a, Num a) => a -> (b -> b) -> b -> b
applyTimes 0 f b = b
applyTimes n f b = f . applyTimes (n - 1) f $ b
