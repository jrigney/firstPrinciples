module Database
  (
  ) where

import Data.Time.Clock

data DatabaseItem
  = DbString String
  | DbNumber Integer
  | DbDate UTCTime
  deriving (Eq, Ord, Show)

mostRecent :: [DatabaseItem] -> UTCTime
mostRecent xs = maximum $ foldr timeItems [] xs

timeItems :: DatabaseItem -> [UTCTime] -> [UTCTime]
timeItems x acc =
  case x of
    DbDate time -> time : acc
    _ -> acc

sumDb :: [DatabaseItem] -> Integer
sumDb = sum . numbers

sumItems :: DatabaseItem -> [Integer] -> [Integer]
sumItems x acc =
  case x of
    DbNumber x -> x : acc
    _ -> acc

numbers :: [DatabaseItem] -> [Integer]
numbers = foldr sumItems []

-- ??? can't get this to typecheck
avgDb :: [DatabaseItem] -> Double
avgDb xs = fromIntegral (sumDb xs) / fromIntegral (length num)
  where
    num = numbers xs
