# ch10

fold is a catamorphism - destruction of data.  reduction of values

a never ending evaluation is a bottom or undefined

## foldr is right associative foldl is left associative

Notice how foldr `foldr f z (x:xs) = f x (foldr f z xs)` has the recusive call on the right side
and `foldl f acc (x:xs) = foldl f (f acc x) xs` has the recursive call on the left.


## cool trick to see how fold associates
```
xs = map show [1..5]
showFold x y = concat ["(",x,"+",y,")"]

showFoldr = foldr showFold "0" xs
showFoldl = foldl showFold "0" xs
```

scanr and scanl can also be used to see how a fold evaluates

## folds and associativity



```
scanr (+) 0 [1..5]
[15,14,12,9,5,0]
```

## foldr

```
foldr (+) 0 (1 : 2 : 3 : [])

(1 + (2 + 3 + 0))
```

### how to evaluate
```
foldr :: (a -> b -> b) -> b -> [a] -> b
foldr f z [] = z
foldr f z (x:xs) = f x (foldr f z xs)
```

Notice how the `foldr f z (x:xs)` above looks just like `(x:xs) -> f x (foldr f z xs)` below.

```
foldr :: (a -> b -> b) -> b -> [a] -> b
foldr f z xs =
  case xs of
    [] -> z
    (x:xs) -> f x (foldr f z xs)
```

Give the definition above this is how it would break down
```
foldr (+) 0 (1 : 2 : 3 : [])
      f   z  x : xs

foldr (+) 0 [1 : [2 , 3]] => (+) 1 foldr (+) 0 [2,3]
foldr (+) 0 [2 : [ 3]] => (+) 2 foldr (+) 0 [3]
foldr (+) 0 [3 : []] => (+) 3 foldr (+) 0 []

so [] evaluates to z which is 0

(+) 1 ((+) 2 ( (+) 3 ((+) [])))
```


### foldr and infinite lists
Because foldr can avoid evaluating both values and the spine.

Since foldr can avoid evaluating all the values and the spine it is possible to use foldr to short circuit.  Meaning foldr can be used to stop evaluation at the first occurance of something.


## foldl
```
foldl (+) 0 (1 : 2 : 3 : [])

(((0 + 1) + 2) + 3)
```
