## Understanding Folds
1. b
2. f = flip (*)
   (((1 'f' 1) 'f' 2) 'f' 3)
3. c
4. a
5a. foldr (++) "" ["woot", "WOOT"]
5b. foldr max '' "fear"
5c. foldr && [False, True]
5d. no
5e. foldl (flip ((++) . show)) "" [1..5]
5f. foldr (flip const) 'a' [1..5]
5g. foldr (flip const) 0 "tacos"
5h. foldl const 0 "burr"
5i. foldl const 'z' [1..5]
