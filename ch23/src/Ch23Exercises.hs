module Ch23Exercises
  (
  ) where

newtype Moi s a = Moi
  { runMoi :: s -> (a, s)
  }

-- fmap :: (a -> b) -> Moi s a -> Moi s b
-- remember g is a function
-- remember Moi takes a function s -> (a,s)
instance Functor (Moi s) where
  fmap f (Moi g) =
    Moi $ \s ->
      let (a, s') = g s
      in (f a, s')

-- pure :: a -> Moi s a
-- (<*>) :: Moi s (a -> b) -> Moi s a -> Moi s b
instance Applicative (Moi s) where
  pure a = Moi $ \s -> (a, s)
  (<*>) (Moi fsab) (Moi gsa) =
    Moi $ \s ->
      let (ab, fs) = fsab s
          (a, gs) = gsa s
      in (ab a, gs)

-- (>>=) :: Moi s a -> (a -> Moi s b) -> Moi s b
instance Monad (Moi s) where
  return = pure
  (>>=) (Moi f) g =
    Moi $ \s ->
      let (a, fs) = f s
      in runMoi (g a) fs

get' :: Moi s s
get' = Moi $ \x -> (x, x)

put' :: s -> Moi s ()
put' s = Moi $ \x -> ((), x)

-- sa is a function 
exec' :: Moi s a -> s -> s
exec' (Moi sa) s = snd $ sa s

eval' :: Moi s a -> s -> a
eval' (Moi sa) = fst . sa

modify' :: (s -> s) -> Moi s ()
modify' f = Moi $ \x -> ((), (f x))
