module FizzBuzz
  (
  ) where

import Control.Monad (mapM_)
import Control.Monad.Trans.State (State, execState, get, put)

fizzBuzz :: Integer -> String
fizzBuzz n
  | n `mod` 15 == 0 = "FizzBuzz"
  | n `mod` 5 == 0 = "Buzz"
  | n `mod` 3 == 0 = "Fizz"
  | otherwise = show n

-- mapM_ :: (Monad m, Foldable t) => (a -> m b) -> t a -> m ()
-- mapM :: (Monad m, Traversable t) => (a -> m b) -> t a -> m (t b)
-- execState :: State s a -> s -> s
-- mapM_ takes a foldable and through a -> m b produces effects and ignores the
-- returned results
-- contrast mapM_ with mapM.
fizzbuzzList :: [Integer] -> [String]
fizzbuzzList list = execState (mapM_ addResult list) []

addResult :: Integer -> State [String] ()
addResult n = do
  xs <- get
  let result = fizzBuzz n
  put (result : xs)

main :: IO ()
main = mapM_ putStrLn $ reverse $ fizzbuzzList [1 .. 100]

fizzbuzzFromTo :: Integer -> Integer -> [String]
fizzbuzzFromTo from to = fizzbuzzList $ enumFromThenTo from (from - 1) to
