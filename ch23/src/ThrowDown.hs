module ThrowDown
  (
  ) where

import Control.Applicative (liftA3)
import Control.Monad (replicateM)
import Control.Monad.Trans.State
import System.Random

data Die
  = DieOne
  | DieTwo
  | DieThree
  | DieFour
  | DieFive
  | DieSix
  deriving (Eq, Show)

intToDie :: Int -> Die
intToDie = undefined

rollDie :: State StdGen Die
rollDie =
  state $ do
    (n, s) <- randomR (1, 6)
    return (intToDie n, s)

-- state (randomR (1, 6)) needs to be evaluated before the (<$>) because
-- (<$>) :: Functor f => (a -> b) -> f a -> f b
-- requires an f a.  
-- state (randomR (1, 6)) returns an StateT s m a.
-- the StateT is what intToDie is being lifted over
rollDie' :: State StdGen Die
rollDie' = intToDie <$> state (randomR (1, 6))
