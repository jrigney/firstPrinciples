module Instances
  (
  ) where

data Identity a =
  Identity a

instance Foldable Identity where
  foldr f z (Identity x) = f x z
  foldMap f (Identity x) = f x

data Optional a
  = Yep a
  | Nada

instance Foldable Optional where
  foldr f z (Yep x) = f x z
  foldr f z (Nada) = z
  foldMap f (Yep x) = f x
  foldMap f Nada = mempty
