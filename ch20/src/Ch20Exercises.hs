module Ch20Exercises
  (
  ) where

data Constant a b =
  Constant b

-- foldr :: Foldable t => (a -> b -> b) -> b -> t a -> b
-- notice the second parameter b can be used as the return b
instance Foldable (Constant a) where
  foldr _ z _ = z

data Two a b =
  Two a
      b

instance Foldable (Two a) where
  foldr f z (Two x y) = f y z

data Three a b c =
  Three a
        b
        c

instance Foldable (Three a b) where
  foldr f z (Three h i j) = f j z

data Three' a b =
  Three' a
         b
         b

-- remember foldr is like an accumulator so get the result of f y' z which is a
-- 'total' and use that as the accumulator for f y
instance Foldable (Three' a) where
  foldr f z (Three' x y y') = f y $ f y' z

data Four' a b =
  Four' a
        b
        b
        b

instance Foldable (Four' a) where
  foldr f z (Four' x y1 y2 y3) = f y1 $ f y2 $ f y3 z

-- foldMap :: (Monoid m, Foldable t) => (a -> m) -> t a -> m
-- the monoid instance allows the foldMap to smash everything together for
-- result of f a
-- so we just need ways to create Applicatives. pure and mempty create
-- Applicatives
filterF :: (Applicative f, Foldable t, Monoid (f a)) => (a -> Bool) -> t a -> f a
filterF f xs =
  foldMap
    (\x ->
       if f x
         then pure x
         else mempty)
    xs
