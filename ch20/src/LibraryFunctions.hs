module LibraryFunctions
  (
  ) where

import Data.Foldable
import Data.Monoid

sum' :: (Foldable t, Num a) => t a -> a
sum' xs = foldr (+) 0 xs

product' :: (Foldable t, Num a) => t a -> a
product' xs = foldr (*) 1 xs

-- Any is the monoid for boolean disjunction
-- Any . (==x) :: boolean -> Any
-- genAny :: Any -> bool
-- foldMap :: (Monoid m, Foldable t) => (a -> m) -> t a -> m
elem' :: (Foldable t, Eq a) => a -> t a -> Bool
elem' x xs = getAny $ foldMap (Any . (== x)) xs

-- null tests if Foldable is empty
minimum' :: (Foldable t, Ord a) => t a -> Maybe a
minimum' xs
  | (not . null) xs = Just $ foldr min initialMin xs
  | otherwise = Nothing
  where
    initialMin = (head . toList) xs

maximum' :: (Foldable t, Ord a) => t a -> Maybe a
maximum' xs
  | (not . null) xs = Just $ foldr max initialMax xs
  | otherwise = Nothing
  where
    initialMax = (head . toList) xs

-- const x :: b -> a
-- a partially applied const returns back a function b -> a so it is handy to
-- use as a function in a higher order function such as foldMap.
-- Any is the monoid for boolean disjunction so foldMap will smash together all
-- the Any's into as single Any
-- mempty::Any is Any {getAny = False}
null' :: (Foldable t) => t a -> Bool
-- null' xs = getAny $ foldMap (\x -> Any False) xs
null' xs = (not . getAny) $ foldMap (const $ (Any True)) xs

length' :: (Foldable t) => t a -> Int
-- length' xs = foldr (\_ acc -> acc + 1) 0 xs
length' xs = getSum $ foldMap (const $ (Sum 1)) xs

toList' :: (Foldable t) => t a -> [a]
toList' xs = foldMap (\x -> [x]) xs

fold' :: (Foldable t, Monoid m) => t m -> m
-- fold' ms = foldMap id ms
fold' ms = foldr (mappend) mempty ms

foldMap' :: (Foldable t, Monoid m) => (a -> m) -> t a -> m
foldMap' f xs = foldr (\n acc -> f n <> acc) mempty xs
