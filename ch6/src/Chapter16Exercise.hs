module Chapter16Exercise
  (
  ) where

-- Multiple choice
-- 1 c
-- 2 b
-- 3 a
-- 4 a
-- 5 b
-- Does it typecheck
-- 1 no - deriving Show
-- 2 no - deriving Eq
-- 3 a Mood
--   b fail different return types x and Blah
--   c fail no instance Ord for Mood
-- 4 yes s1 partial application of data ctor
-- Given data type declaration
-- 1 no - True is not Yeah "chases" is no Rocks
-- 2 yes
-- 3 yes
-- 4 no - no Ord type class
-- Match types
-- 1 no
-- 2 yes
-- 3 yes
-- 4 yes
-- 5 yes
-- 6 yes
-- 7 no
-- 8 no
-- 9 yes
-- 10 yes
-- 11 yes
chk :: Eq b => (a -> b) -> a -> b -> Bool
chk ab x y = (ab x) == y

arith :: Num b => (a -> b) -> Integer -> a -> b
arith ab x y = (ab y) + (fromInteger x)
