module EqInstance
  (
  ) where

-- The type class for Eq just takes a type parameter
-- class Eq a where
-- versus Functor which takes a type constructor *->*
-- class Functor (f :: * -> *) where
-- that is why you need to put only part of the type ctor into the structure of
-- the functor
--
--
-- 1
data TisAnInteger =
  TisAn Integer

instance Eq TisAnInteger where
  (==) (TisAn x) (TisAn y) = x == y

-- 2
data TwoIntegers =
  Two Integer
      Integer

instance Eq TwoIntegers where
  (==) (Two x y) (Two x' y') = x == x' && y == y'

-- 3
data StringOrInt
  = TisAnInt Int
  | TisAString String

instance Eq StringOrInt where
  (==) (TisAnInt x) (TisAnInt y) = x == y
  (==) (TisAString x) (TisAString y) = x == y
  (==) (TisAnInt _) (TisAString _) = False
  (==) (TisAString _) (TisAnInt _) = False

-- 4
data Pair a =
  Pair a

instance Eq a => Eq (Pair a) where
  (==) (Pair x) (Pair y) = x == y

-- 5
data Tuple a b =
  Tuple a
        b

instance (Eq a, Eq b) => Eq (Tuple a b) where
  (==) (Tuple x y) (Tuple x' y') = x == x' && y == y'

-- 6
data Which a
  = ThisOne a
  | ThatOne a

instance (Eq a) => Eq (Which a) where
  (==) (ThisOne x) (ThisOne y) = x == y
  (==) (ThatOne x) (ThatOne y) = x == y
  (==) (ThisOne _) (ThatOne _) = False
  (==) (ThatOne _) (ThisOne _) = False

-- 7
data EitherOr a b
  = Hello a
  | Goodbye b

instance (Eq a, Eq b) => Eq (EitherOr a b) where
  (==) (Hello x) (Hello y) = x == y
  (==) (Goodbye x) (Goodbye y) = x == y
  (==) (Hello _) (Goodbye _) = False
  (==) (Goodbye _) (Hello _) = False
