module ComposeInstances
  (
  ) where

-- foldMap :: (Monoid m, Foldable t) => (a -> m) -> t a -> m
-- foldMap am :: t a -> m
-- fga is Foldable since it is defined in the instance as Foldable f
-- so the ga will play the role of a for a -> m
instance (Foldable f, Foldable g) => Foldable (Compose f g) where
  foldMap am (Compose fga) = foldMap (foldMap am) fga

-- traverse :: (Applicative f, Traversable t) => (a -> f b) -> t a -> f (t b)
-- traverse f :: t a -> f (t b)
--               a   -> f  b
-- traverse (traverse f) fga :: f1 (f (g b))
-- fga is Traversable since it is defined in the instance as Traversable f
-- Compose <$> traverse (traverse f) fga :: f1 (Compose f g b)
instance (Traversable f, Traversable g) => Traversable (Compose f g) where
  traverse f (Compose fga) = Compose <$> traverse (traverse f) fga

instance (Functor f, Functor g) => Functor (Compose f g) where
  fmap f (Compose fga) = Compose $ (fmap . fmap) f fga

instance (Applicative f, Applicative g) => Applicative (Compose f g) where
  pure = Compose . pure . pure
  (Compose f) <*> (Compose a) = Compose $ (<*>) <$> f <*> a

newtype Compose f g a = Compose
  { getCompose :: f (g a)
  } deriving (Eq, Show)
