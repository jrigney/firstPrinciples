{-# LANGUAGE InstanceSigs #-}

-- InstanceSigs Allow type signatures for members in instance definitions
module Gotcha
  (
  ) where

import Control.Applicative (liftA2)

instance (Functor f, Functor g) => Functor (Compose f g) where
  fmap f (Compose fga) = Compose $ (fmap . fmap) f fga

instance (Applicative f, Applicative g) => Applicative (Compose f g) where
  pure :: a -> Compose f g a
  -- (<*>) :: Functor f => f (a -> b) -> f a -> f b
  -- f and g need to be Applicative so pure gets an applicative
  -- Compose is a function f(g a) we need to get to that function
  pure = Compose . pure . pure
  -- (<*>) :: Functor f => f (a -> b) -> f a -> f b
  -- (<$>) :: Functor f => (a -> b) -> f a -> f b
  -- f = f (g (a -> b))
  -- (<*>) <$> f = f (g a -> g b)
  -- a = f (g a)
  -- (<*>) <$> f <*> a = f (g b)
  (<*>) :: Compose f g (a -> b) -> Compose f g a -> Compose f g b
  (Compose f) <*> (Compose a) = Compose $ (<*>) <$> f <*> a
  -- (Compose f) <*> (Compose a) = Compose $ liftA2 (<*>) f a

newtype Compose f g a = Compose
  { getCompose :: f (g a)
  } deriving (Eq, Show)

newtype Identity a = Identity
  { runIdentity :: a
  }
