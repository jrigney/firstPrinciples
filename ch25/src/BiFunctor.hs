module BiFunctor
  (
  ) where

class Bifunctor p where
  {-# MINIMAL bimap | first, second #-}
  bimap :: (a -> b) -> (c -> d) -> p a c -> p b d
  bimap f g = first f . second g
  first :: (a -> b) -> p a c -> p b c
  first f = bimap f id
  second :: (b -> c) -> p a b -> p a c
  second = bimap id

data Deux a b =
  Deux a
       b

instance Bifunctor Deux where
  bimap f g (Deux x y) = Deux (f x) (g y)

data Const a b =
  Const a

instance Bifunctor Const where
  bimap f g (Const x) = Const (f x)

data Drei a b c =
  Drei a
       b
       c

-- Bifunctor takes a type with two type parameters because it's use as p in
-- bimap has two type parameters
-- ignore the letters pay attention to the position in bimap
-- Drei a is a partially applied type so the b and c in the type Drei a b c
-- become the a and be in the context of Bifunctor and bimap
instance Bifunctor (Drei a) where
  bimap ab cd (Drei x a c) = Drei x (ab a) (cd c)

data SuperDrei a b c =
  SuperDrei a
            b

instance Bifunctor (SuperDrei a) where
  bimap ab _ (SuperDrei x a) = SuperDrei x (ab a)

data SemiDrei a b c =
  SemiDrei a

instance Bifunctor (SemiDrei a) where
  bimap _ _ (SemiDrei a) = SemiDrei a

data Quadriceps a b c d =
  Quadzzz a
          b
          c
          d

instance Bifunctor (Quadriceps a b) where
  bimap ab cd (Quadzzz x y a c) = Quadzzz x y (ab a) (cd c)

data Either' a b
  = Left' a
  | Right' b

instance Bifunctor Either' where
  bimap ab _ (Left' a) = Left' (ab a)
  bimap _ cd (Right' c) = Right' (cd c)
