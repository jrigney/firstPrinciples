# ch22

## But uh, Reader?

```
newtype Reader r a =
  Reader {runReader :: r -> a}

instance Functor (Reader r) where
  fmap :: (a -> b) -> Reader r a -> Reader r b
  fmap f (Reader ra) = Reader $ \r -> f (ra r)
```

Remember what is after the `::` are types.  So `Reader r a` is the type that matches
the newtype definition.  The newtype definition also says Reader has a function as its value.
That makes `ra` in `Reader ra` a function.

Remember fmap?
```
fmap :: Functor f => (a -> b) -> f a -> f b
```

Here is how it lines up with the Reader instance above
```
fmap ::              (a -> b) -> Reader r a -> Reader r b
fmap :: Functor f => (a -> b) -> f      a   -> f      b
```

So that means the value that `ra` returns is of type `a`

`Reader r b` need to 'contain' a function of r -> b




