{-# LANGUAGE InstanceSigs #-}

module ReadingComp
  (
  ) where

-- fmap :: Functor f => (a -> b) -> f a -> f b
-- (<*>) :: Applicative f => f (a -> b) -> f a -> f b
-- f <$> x will partially apply (a -> b -> c) with f a and give you f (b -> c)
-- whenever you have a function in a structure as f (b -> c) think of (<*>)
myLiftA2 :: Applicative f => (a -> b -> c) -> f a -> f b -> f c
myLiftA2 f x y = f <$> x <*> y

newtype Reader r a = Reader
  { runReader :: r -> a
  }

asks :: (r -> a) -> Reader r a
asks f = Reader f

-- ra is a function that returns type a as defined by Reader above
-- the Reader constructor takes a function
-- f need to transform the return from ra
instance Functor (Reader r) where
  fmap f (Reader ra) = Reader $ \x -> f (ra x)

-- Reader r (a -> b) == Reader (r -> (a -> b))
instance Applicative (Reader r) where
  pure :: a -> Reader r a
  pure a = Reader $ const a
  (<*>) :: Reader r (a -> b) -> Reader r a -> Reader r b
  (Reader rab) <*> (Reader ra) = Reader $ \r -> rab r (ra r)
