module WarmingUp
  (
  ) where

import Control.Applicative (liftA2)
import Data.Char

cap :: [Char] -> [Char]
cap xs = map toUpper xs

rev :: [Char] -> [Char]
rev xs = reverse xs

composed :: [Char] -> [Char]
composed = cap . rev

-- fmap is function compose
-- cap is 'lifted' into rev
fmapped :: [Char] -> [Char]
fmapped = cap <$> rev

boop = (* 2)

doop = (+ 10)

--similar to fmapped
--boop is 'lifted' over the partially applied function doop
fmapped' :: Integer -> Integer
fmapped' = boop <$> doop

--notice how liftA2 is equivalent to <*> <$>
tupled :: [Char] -> ([Char], [Char])
tupled = (,) <*> cap <$> rev
--tupled = liftA2 (,) cap rev
