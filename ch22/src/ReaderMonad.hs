{-# LANGUAGE InstanceSigs #-}

module ReaderMonad
  (
  ) where

newtype Reader r a = Reader
  { runReader :: r -> a
  }

instance Functor (Reader r) where
  fmap f (Reader ra) = Reader $ \x -> f (ra x)

-- (<*>) Applicative f => f (a -> b) -> f a -> f b
instance Applicative (Reader r) where
  pure x = Reader (const x)
  (<*>) (Reader ab) (Reader ra) = Reader $ \r -> ab r (ra r)

-- (>>=) :: Monad m => m a -> (a -> m b) -> m b
instance Monad (Reader r) where
  return = pure
  (>>=) :: Reader r a -> (a -> Reader r b) -> Reader r b
  (Reader ra) >>= aRb = Reader $ \r -> (runReader (aRb (ra r))) r

-- Reader encapsulates the function that will get the needed value when needed
getDogRM :: Reader Person Dog
getDogRM = Dog <$> (Reader dogName) <*> (Reader address)

data Person = Person
  { humanName :: HumanName
  , dogName :: DogName
  , address :: Address
  } deriving (Eq, Show)

data Dog = Dog
  { dogsName :: DogName
  , dogsAddress :: Address
  } deriving (Eq, Show)

newtype HumanName =
  HumanName String
  deriving (Eq, Show)

newtype DogName =
  DogName String
  deriving (Eq, Show)

newtype Address =
  Address String
  deriving (Eq, Show)
