module NewBeginning
  (
  ) where

import Control.Applicative

boop :: (Num a) => a -> a
boop = (* 2)

doop :: (Num a) => a -> a
doop = (+ 10)

bip :: (Num a) => a -> a
bip = boop . doop

-- Functor of functions
bloop :: Integer -> Integer
bloop = fmap boop doop

-- Applicative context
-- here the argument will get passed to both boop and doop and then the results
-- will get added together
bbop :: Integer -> Integer
bbop = (+) <$> boop <*> doop

-- Applicative context
duwop :: Integer -> Integer
duwop = liftA2 (+) boop doop
