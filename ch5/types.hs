data Trio = TrioOf Int Char Bool

getInt :: Trio -> Int
getInt (TrioOf i _ _ ) = i

data Sum = One | Two | Three | Four

data Bool = True | False

data Same = Same Bool Sum

data Person = Person 
  { personName :: String
  , personAge :: Int
  } deriving (Eq, Show)

data Dog = Dog
  { dogName :: String
  , dogOwner :: String
  }

type Record xs = ()

getName :: Contains ("name" =: a) xs 
        => Record xs 
        -> a
getName = get "name"
