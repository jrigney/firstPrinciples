--Screen RC2 pg. 140
module Parametric where

--1
myid :: a -> a
myid x = x

--2
myid2 :: a -> a -> a
myid2 x y = x

myid3 :: a -> a -> a
myid3 x y = x

--3
changes :: a -> b -> b
changes x y = y
