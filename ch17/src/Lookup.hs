module Lookup
  (
  ) where

import Data.List

--
added :: Maybe Integer
added = (+ 3) <$> (lookup 3 $ zip [1, 2, 3] [4, 5, 6])

--- 2
y :: Maybe Integer
y = lookup 3 $ zip [1 .. 3] [4 .. 6]

z :: Maybe Integer
z = lookup 2 $ zip [1 .. 3] [4 .. 5]

-- partially apply (,)
-- remember fmap (a -> b) -> f a -> f b so f b will be Maybe(b -> (a,b)) where
-- 'a' is the partially applied part.
tupled :: Maybe (Integer, Integer)
tupled = (,) <$> y <*> z

--- 3
x :: Maybe Int
x = elemIndex 3 [1 .. 5]

y' :: Maybe Int
y' = elemIndex 4 [1 .. 5]

max' :: Int -> Int -> Int
max' = max

maxed :: Maybe Int
maxed = max' <$> x <*> y'

--- 4
xs = [1 .. 3]

ys = [4 .. 5]

x' :: Maybe Integer
x' = lookup 3 $ zip xs ys

y'' :: Maybe Integer
y'' = lookup 2 $ zip xs ys

-- applying fmap (<$>) to a function that takes 2 arguments and applying
-- tiefighter (<*>) is like applying both parameters to the function.
summed :: Maybe Integer
summed = pure sum <*> ((,) <$> x' <*> y'')
--summed' = fmap sum $ (,) <$> x' <*> y''
