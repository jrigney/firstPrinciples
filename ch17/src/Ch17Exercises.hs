module Ch17Exercises
  (
  ) where

import Control.Applicative (liftA3)
import Data.Monoid ((<>))
import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

--1
--pure :: [a]
--(<*>) [(a->b)] -> [a] -> [b]
--
--2
--pure :: IO a
--(<*>) :: IO(a->b) -> IO a -> IO b
--
--3
--pure :: a -> (,) a a
--(<*>) :: (,) a (a->b) -> (,) a a -> (,) a b
--
--4
--pure :: a -> (->) e a
--(<*>) :: (->)a (a->b) -> (->)a a -> (->)a b
data Pair a =
  Pair a
       a
  deriving (Eq, Show)

instance Functor Pair where
  fmap f (Pair x y) = Pair (f x) (f y)

instance Applicative Pair where
  pure x = Pair x x
  (<*>) (Pair fx fy) (Pair x y) = Pair (fx x) (fy x)

instance Arbitrary a => Arbitrary (Pair a) where
  arbitrary = do
    a <- arbitrary
    return $ Pair a a

instance Eq a => EqProp (Pair a) where
  (=-=) = eq

testPair = quickBatch $ applicative (undefined :: Pair (Int, Double, Char))

--Two
data Two a b =
  Two a
      b
  deriving (Eq, Show)

instance Functor (Two a) where
  fmap f (Two x y) = Two x (f y)

-- to combine the a the only thing we have is what is available from Monoid
-- which is mappend
instance Monoid a => Applicative (Two a) where
  pure x = Two mempty x
  (<*>) (Two fx fy) (Two x y) = Two (fx <> x) (fy y)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Two a b) where
  arbitrary = do
    x <- arbitrary
    y <- arbitrary
    return $ Two x y

instance (Eq a, Eq b) => EqProp (Two a b) where
  (=-=) = eq

testTwo = quickBatch $ applicative (undefined :: Two String (Int, Double, Char))

-- Three a b c
data Three a b c =
  Three a
        b
        c
  deriving (Eq, Show)

instance Functor (Three a b) where
  fmap f (Three x y z) = Three x y (f z)

instance (Monoid a, Monoid b) => Applicative (Three a b) where
  pure x = Three mempty mempty x
  (<*>) (Three fx fy fz) (Three x y z) = Three (fx <> x) (fy <> y) (fz z)

instance (Arbitrary a, Arbitrary b, Arbitrary c) => Arbitrary (Three a b c) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    c <- arbitrary
    return $ Three a b c

instance (Eq a, Eq b, Eq c) => EqProp (Three a b c) where
  (=-=) = eq

testThree = quickBatch $ applicative (undefined :: Three String String (Int, Double, Char))

--Three' a b
data Three' a b =
  Three' a
         a
         b
  deriving (Eq, Show)

instance Functor (Three' a) where
  fmap f (Three' x y z) = Three' x y (f z)

instance (Monoid a) => Applicative (Three' a) where
  pure x = Three' mempty mempty x
  (<*>) (Three' x y z) (Three' x1 y1 z1) = Three' (x <> x1) (y <> y1) (z z1)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Three' a b) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    return $ Three' a a b

instance (Eq a, Eq b) => EqProp (Three' a b) where
  (=-=) = eq

testThree' = quickBatch $ applicative (undefined :: Three' String (Int, Double, Char))

-- Combinations
stops :: String
stops = "pbtdkg"

vowels :: String
vowels = "aeiou"

-- think about what you want the function to return.
-- you already know that the structure will be [] so you want a tuple
-- look at the type of liftA3 :: Applicative f => (a -> b -> c -> d) -> f a -> f b -> f c -> f d
-- notice it takes just a function not a function in a structure.
combos :: [a] -> [b] -> [c] -> [(a, b, c)]
combos as bs cs = liftA3 (,,) as bs cs
