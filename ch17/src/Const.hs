module Const
  (
  ) where

import Data.Monoid

newtype Constant a b = Constant
  { getConstant :: a
  } deriving (Eq, Show)

instance Functor (Constant a) where
  fmap f (Constant x) = Constant x

-- ??? not sure how to explain just guessed since 'a' is Monoid
instance Monoid a => Applicative (Constant a) where
  pure x = Constant mempty
  (<*>) (Constant f) (Constant y) = Constant $ f <> y
