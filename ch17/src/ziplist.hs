-- Implement the ZipList Applicative. Use the checkers library to validate
-- your Applicative instance. We're going to provide the EqProp instance
-- and explain the weirdness in a moment.
import Control.Applicative (liftA2)
import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

data List a =
    Nil
  | Cons a (List a)
  deriving (Eq, Show)

take' :: Int -> List a -> List a
take' _ Nil = Nil
take' 0 _   = Nil
take' i (Cons a as) = Cons a (take' (i-1) as)

append :: List a -> List a -> List a
append Nil ys = ys
append (Cons x xs) ys =
  Cons x $ xs `append` ys

fold :: (a -> b -> b) -> b -> List a -> b
fold _ b Nil = b
fold f b (Cons h t) = f h (fold f b t)

concat' :: List (List a) -> List a
concat' = fold append Nil

replicateM'        :: (Applicative m) => Int -> m a -> m (List a)
replicateM' cnt0 f =
    loop cnt0
  where
    loop cnt
        | cnt <= 0  = pure Nil
        | otherwise = liftA2 Cons f (loop (cnt - 1))

vectorOf' :: Int -> Gen a -> Gen (List a)
vectorOf' = replicateM'

-- | Generates a list of random length. The maximum
--   length depends on the size parameter.
listOf' :: Gen a -> Gen (List a)
listOf' gen = sized $ \n ->
  do k <- choose (0,n)
     vectorOf' k gen

instance (Eq a, EqProp a) => EqProp (List a) where (=-=) = eq
instance Arbitrary a => Arbitrary (List a) where
  arbitrary =
    listOf' arbitrary

instance Monoid (List a) where
  mempty = Nil
  mappend = append

instance Functor List where
  fmap _ Nil = Nil
  fmap f (Cons a as) = Cons (f a) (fmap f as) 

instance Applicative List where
  pure a = Cons a Nil 
  (<*>) Nil _ = Nil
  (<*>) (Cons f fs) xs = fmap f xs `append` (fs <*> xs) 

newtype ZipList' a = 
  ZipList' (List a)
  deriving (Eq, Show)

instance Eq a => EqProp (ZipList' a) where 
  xs =-= ys = xs' `eq` ys'
    where xs' = let (ZipList' l) = xs
                in take' 3000 l
          ys' = let (ZipList' l) = ys
                in take' 3000 l

instance Functor ZipList' where
  fmap f (ZipList' xs) = 
    ZipList' $ fmap f xs

repeat' :: a -> List a
repeat' a = Cons a (repeat' a)

zipWith' :: (a -> b -> c) -> List a -> List b -> List c
zipWith' _ Nil _ = Nil
zipWith' _ _ Nil = Nil
zipWith' f (Cons a as) (Cons b bs) = Cons (f a b) $ zipWith' f as bs

instance Applicative ZipList' where
  pure a = ZipList' (repeat' a)
  (<*>) (ZipList' Nil) _ = ZipList' Nil  
  (<*>) (ZipList' fs) (ZipList' as) = ZipList' $ zipWith' ($) fs as 

main :: IO ()
main = do
  let n :: Int; n = 1
      xs = Cons (n, n, n) Nil
  quickBatch (monoid xs)
  quickBatch (applicative xs)
