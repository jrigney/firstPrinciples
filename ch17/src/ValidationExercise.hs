module ValidationExercise
  (
  ) where

import Data.Monoid ((<>))
import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

data Validation e a
  = Failure' e
  | Success' a
  deriving (Eq, Show)

instance Functor (Validation e) where
  fmap _ (Failure' e) = Failure' e
  fmap f (Success' a) = Success' $ f a

instance Monoid e => Applicative (Validation e) where
  pure x = Success' x
  (<*>) (Success' f) (Success' a) = Success' $ f a
  (<*>) (Success' f) (Failure' a) = Failure' a
  (<*>) (Failure' f) (Success' a) = Failure' f
  (<*>) (Failure' f) (Failure' a) = Failure' $ f <> a

instance (Eq e, Eq a) => EqProp (Validation e a) where
  (=-=) = eq

-- quickBatch $ applicative (Success' ("a", "b", 1))
-- TestBatch's Applicative instance will create function a to b and b to c so
-- we need to specify the types inside of the type we created an instance for.
-- In this case Validation e a.
instance (Arbitrary e, Arbitrary a) => Arbitrary (Validation e a) where
  arbitrary = do
    a <- arbitrary
    e <- arbitrary
    elements [Failure' e, Success' a]
