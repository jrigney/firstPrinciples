module IdentityInstance
  (
  ) where

-- the identity is used to create structure without changing the semantics
-- look at the following when structure does not exist but would be helpful
xs = [1 .. 3]

xs' = [9, 9, 9]

-- below returns [1,1,1,2,2,2,3,3,3] because the const is being lifted over the
-- list so it is being applied to 1,2,3 and 9,9.9
--const <$> xs <*> xs'
-- now apply a little structure in the form of an Identity
-- below returns [1,2,3]
--const <$> Identity xs <*> Identity xs'
newtype Identity a =
  Identity a
  deriving (Eq, Show)

instance Functor Identity where
  fmap f (Identity a) = Identity $ f a

instance Applicative Identity where
  pure a = Identity a
  (<*>) (Identity f) (Identity a) = Identity $ f a
