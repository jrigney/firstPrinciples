module Ch18Exercises
  (
  ) where

import Control.Monad (join, liftM2)
import Data.Monoid ((<>))
import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

-- 1 ------------------------
data Nope a =
  NopeDotJpg
  deriving (Eq, Show)

instance Functor Nope where
  fmap _ _ = NopeDotJpg

instance Applicative Nope where
  pure _ = NopeDotJpg
  _ <*> _ = NopeDotJpg

instance Monad Nope where
  return _ = NopeDotJpg
  _ >>= f = NopeDotJpg

instance Arbitrary (Nope a) where
  arbitrary = return NopeDotJpg

instance EqProp (Nope a) where
  (=-=) = eq

testNopeFunctor = quickBatch $ functor (undefined :: Nope (Int, [String], String))

testNopeApplicative = quickBatch $ applicative (undefined :: Nope (Int, [String], String))

testNopeMonad = quickBatch $ monad (undefined :: Nope (Int, [String], String))

-- 2 ------------------------
data PhhhbbtttEither b a
  = Left' a
  | Right' b
  deriving (Eq, Show)

instance Functor (PhhhbbtttEither b) where
  fmap _ (Right' x) = Right' x
  fmap f (Left' x) = Left' $ f x

instance Applicative (PhhhbbtttEither b) where
  pure x = Left' x
  Left' f <*> x = f <$> x
  Right' x <*> _ = Right' x

instance Monad (PhhhbbtttEither b) where
  return x = Left' x
  Left' x >>= f = f x
  Right' x >>= _ = Right' x

instance (Arbitrary b, Arbitrary a) => Arbitrary (PhhhbbtttEither b a) where
  arbitrary = do
    b <- arbitrary
    a <- arbitrary
    elements [Left' a, Right' b]

instance (Eq b, Eq a) => EqProp (PhhhbbtttEither b a) where
  (=-=) = eq

testPFunctor =
  quickBatch $
  functor (undefined :: PhhhbbtttEither (Int, [String], String) (Int, [String], String))

testPApplicative =
  quickBatch $
  applicative (undefined :: PhhhbbtttEither (Int, [String], String) (Int, [String], String))

testPMonad =
  quickBatch $ monad (undefined :: PhhhbbtttEither (Int, [String], String) (Int, [String], String))

-- 3 -----------------------------
newtype Identity a =
  Identity a
  deriving (Eq, Ord, Show)

instance Functor Identity where
  fmap f (Identity x) = Identity $ f x

instance Applicative Identity where
  pure x = Identity x
  Identity f <*> x = f <$> x

instance Monad Identity where
  return x = Identity x
  Identity x >>= f = f x

instance (Arbitrary a) => Arbitrary (Identity a) where
  arbitrary = do
    a <- arbitrary
    return $ Identity a

instance (Eq a) => EqProp (Identity a) where
  (=-=) = eq

testIdFunctor = quickBatch $ functor (undefined :: Identity (Int, [String], String))

testIdApplicative = quickBatch $ applicative (undefined :: Identity (Int, [String], String))

testIdMonad = quickBatch $ monad (undefined :: Identity (Int, [String], String))

-- 4 ------------------
data List a
  = Nil
  | Cons a
         (List a)
  deriving (Eq, Show)

instance Monoid (List a) where
  mempty = Nil
  mappend Nil x = x
  mappend x Nil = x
  mappend (Cons x xs) ys = (Cons x (xs `mappend` ys))

instance Functor List where
  fmap _ Nil = Nil
  fmap f (Cons x xs) = Cons (f x) (fmap f xs)

instance Applicative List where
  pure x = Cons x Nil
  (Cons f fs) <*> xs = (f <$> xs) <> (fs <*> xs)
  Nil <*> _ = Nil

instance Monad List where
  return = pure
  (Cons x xs) >>= f = (f x) <> (xs >>= f)
  Nil >>= _ = Nil

instance Arbitrary a => Arbitrary (List a) where
  arbitrary = do
    x <- arbitrary
    y <- arbitrary
    frequency [(1, return Nil), (3, return (Cons x y))]

instance (Eq a) => EqProp (List a) where
  (=-=) = eq

testListFunctor = quickBatch $ functor (undefined :: List (Int, [String], String))

testListApplicative = quickBatch $ applicative (undefined :: List (Int, [String], String))

testListMonad = quickBatch $ monad (undefined :: List (Int, [String], String))

-- Method provided by Monad and Functor
--1 ----------------
j :: Monad m => m (m a) -> m a
j = join

--2
l1 :: Monad m => (a -> b) -> m a -> m b
l1 = fmap

--3
l2 :: Monad m => (a -> b -> c) -> m a -> m b -> m c
l2 = liftM2

--4
a :: Monad m => m a -> m (a -> b) -> m b
a = flip (<*>)

--5
-- just using <> will append the m b which is not what we want we
-- want to append the b's together so we need a (:) operator
-- when we apply the (:) operator to (f x) we get back a partially applied
-- function in an m so we can now  apply (<*>).
meh :: Monad m => [a] -> (a -> m b) -> m [b]
meh [] _ = return []
meh (x:xs) f = (:) <$> (f x) <*> (meh xs f)

-- meh (a:as) f = do
--   b <- f a
--   bs <- meh as f
--   return (b : bs)
--6
flipType :: (Monad m) => [m a] -> m [a]
flipType xs = meh xs id
