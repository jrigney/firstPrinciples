module EitherMonad
  (
  ) where

import Control.Monad (join)

data Sum a b
  = First a
  | Second b
  deriving (Eq, Show)

instance Functor (Sum a) where
  fmap _ (First x) = First x
  fmap f (Second x) = Second $ f x

instance Applicative (Sum a) where
  pure a = Second a
  -- (Second f) <*> x = f <$> x
  (First f) <*> _ = First f
  (Second _) <*> (First a) = First a
  (Second f) <*> (Second a) = Second $ f a

instance Monad (Sum a) where
  return = pure
  (First x) >>= _ = First x
  (Second x) >>= f = f x
