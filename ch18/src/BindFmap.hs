module BindFmap
  (
  ) where

import Control.Monad (join)

bind :: Monad m => (a -> m b) -> m a -> m b
bind amb ma = join $ fmap amb ma
