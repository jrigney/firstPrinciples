module Ch7Exercises
  (
  ) where

--Multiple choice
--1 d
--2 b
--3 d
--4 b
--5 a
--
-- Write Code
-- 1
tensDigit :: Integral a => a -> a
tensDigit x = d
  where
    xLast = x `div` 10
    d = xLast `mod` 10

-- create a divMod function that can take the numerator as the parameter so we
-- need to use flip because divMod numerator divisor
-- (.) :: (y -> z) -> (x -> y) -> (x -> z)
-- fst :: (h, i) -> h
-- snd :: (j,k) -> k
-- d :: m -> (n,o)
--
-- (fst . d) :: ((h,i) -> h) -> (x -> (h,i)) -> (x -> h)
--               (y    -> z) -> (x ->  y   ) -> (x -> z)
--              ((h,i) -> h) -> (m -> (n,o)) -> (m -> h)
--               (y    -> h) -> (x ->  y   ) -> (x -> z)
--          so  (m -> h) where m is the parameter to d and h is the first of the
--          tuple
-- (snd . d) :: ((j,k) -> k) -> (x -> (j,k)) -> (x -> k)
--               (y    -> z) -> (x ->  y   ) -> (x -> z)
--              ((j,k) -> k) -> (m -> (n,o)) -> (m -> k)
--               (y    -> h) -> (x ->  y   ) -> (x -> z)
--          so (m -> k) where m is the parameter to d and k is the second of the
--          tuple
--(snd . d) . (fst . d) :: (m -> k) -> (x -> m) -> (x -> k) --sub snd . d
--                         (y -> z) -> (x -> y) -> (x -> z)
--                         (m -> k) -> (q -> o) -> (q -> k) --sub fst . d
--                         (y -> z) -> (x -> y) -> (x -> z)
--                  so (q -> k) where q is parameter to snd . d which is
--                  parameter to d Note q->o is m->k from snd . d.
--                  and k is second of the tuple.
--
-- In other words
-- (fst . d) 123 returns 12
-- 12 divMod 10 returns (1,2)
-- (snd . d) 12 returns 2
--
tensDigit' :: Integral a => a -> a
tensDigit' x = (snd . d) . (fst . d) $ x
  where
    d = flip divMod 10

hunsD' :: Integral a => a -> a
hunsD' x = (snd . d) . (fst . d) . (fst . d) $ x
  where
    d = flip divMod 10

--2
foldBool :: a -> a -> Bool -> a
foldBool x y z =
  case z of
    True -> x
    False -> y

foldBool' :: a -> a -> Bool -> a
foldBool' x y z
  | z == True = x
  | z == False = y

--3
g :: (a -> b) -> (a, c) -> (b, c)
g f (x, y) = (f x, y)
