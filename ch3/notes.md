## 
whenever you see :: you are looking at a type signature

main is the default action build a file or run in ghci

## stack
having a main in main.hs is required

## writing haskell
* define values at the top level of a module to make sure they are available
  through out the module

* specify explicit types for top-level definitions

## top-level
top-level definitions are not nested in anything else and are in scope of the
whole module

## operators

### concatination
(++) - combines two lists into a single list
:t (++) [a] -> [a] -> [a]

concat - takes a list of list and flattens it into a single list
:t concat [[a]] -> [a]

### list operators
common in prelude but considered unsafe. meaning they throw an exception when the list is empty
(:) - cons
head
tail
take
drop
(!!) - return element as specified position

## ghci
:t find the type of a term or expression
:m load/unload a module
:set prompt "> " to set the prompt to >


## exercise: Scope
1. yes
2. no
3. yes a file does not order dependant
4. yes

## exercise: Syntax Errors
1. fails - ++ is an infix operator (++) [1,2,3] [4,5,6]
2. fails single quotes are for chars "<3" ++ " Haskell"
3. compiles

## Chapter exercise
### Reading syntax
#### 1
a. [1,2,3,4,5,6]
b. [1,2,3,4,5,6]
c. "hello world"
d. fails ["hello" ++ "world"]
e. fails "hello" !! 4
f. o
g. fail take 4 "lovely"
h. awe

#### 2
a -> d
b -> c
c -> e
d -> a
e -> b

## Building functions
### 1
a. "Curry is awesome" ++ "!"
b. drop 4 $ take 5 "Curry is awesome"
c. drop 9 "Curry is awesome"



