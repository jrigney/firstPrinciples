module Reverse where

rvrs :: [Char] -> [Char]
rvrs xs = let awesome = drop 9 xs
              is = (drop 5 $ take 9 xs)
              curby = take 5 xs
          in awesome ++ is ++ curby

cia = "Curby is awesome"
main :: IO()
main = print $ rvrs cia
