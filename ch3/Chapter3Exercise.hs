module Chapter3Exercise where

bang :: [Char] -> [Char]
bang xs = xs ++ "!"

drop4 :: [Char] -> [Char]
drop4 xs = drop 4 $ take 5 xs

drop9 :: [Char] -> [Char]
drop9 xs = drop 9 xs

cia = "Curby is awesome"

-- Building Functions 3
thirdLetter :: String -> Char
thirdLetter x = head $ drop 1 $ take 2 x

-- Building Functions 4
letterIndex :: Int -> Char
letterIndex x = head $ drop (x-1) $ take x cia

-- Building Functions 5
rvrs :: [Char] -> [Char]
rvrs xs = let awesome = drop 9 xs
              is = (drop 5 $ take 9 xs)
              curby = take 5 xs
          in awesome ++ is ++ curby


