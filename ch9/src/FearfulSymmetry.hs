module FearfulSymmetry
  (
  ) where

myWords :: [Char] -> [[Char]]
myWords [] = []
myWords xs =
  let wordsFrom = takeWhile (/= ' ') xs
      restWords = dropWhile (== ' ') $ dropWhile (/= ' ') xs
  in wordsFrom : myWords restWords
