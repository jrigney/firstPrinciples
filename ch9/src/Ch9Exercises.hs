module Ch9Exercises
  (
  ) where

import Data.Char

firstCapital :: [Char] -> [Char]
firstCapital (x:xs) = toUpper x : xs

-- standard functions
myOr :: [Bool] -> Bool
myOr [] = False
myOr (x:xs) =
  if x == True
    then True
    else myOr xs

myAny :: (a -> Bool) -> [a] -> Bool
myAny _ [] = False
myAny f (x:xs) =
  if f x == True
    then True
    else myAny f xs

squish :: [[a]] -> [a]
squish [] = []
squish (xs:xss) = xs ++ squish xss

squishMap :: (a -> [b]) -> [a] -> [b]
squishMap _ [] = []
--squishMap fxys xs = squish $ (map (fxys) xs)
squishMap fxys xs =
  let go _ [] = []
      go f (x:xs) = f x : go f xs
  in concat $ go fxys xs

squishAgain :: [[a]] -> [a]
squishAgain [] = []
squishAgain xs = squishMap id xs

myMaximumBy :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy _ [] = undefined
myMaximumBy f (x:xs) =
  let go [] acc = acc
      go (x:xs) acc =
        if (f acc x == GT)
          then go xs acc
          else go xs x
  in go xs x

myMinimumBy :: (a -> a -> Ordering) -> [a] -> a
myMinimumBy _ [] = undefined
myMinimumBy f (x:xs) =
  let go [] acc = acc
      go (x:xs) acc =
        if (f acc x == LT)
          then go xs acc
          else go xs x
  in go xs x

myMaximum :: (Ord a) => [a] -> a
myMaximum xs = myMaximumBy compare xs

myMinimum :: (Ord a) => [a] -> a
myMinimum xs = myMinimumBy compare xs
