module MoreBottoms
  (
  ) where

import Data.Bool

moreBool :: a -> a -> Bool -> [a] -> [a]
moreBool x y z xs = map (\x -> bool x y z) xs
