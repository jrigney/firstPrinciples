module ComprehendLists
  (
  ) where
-- list comprehension generates a new list from lists.
-- there must be at least one generator that gives the input for the
-- comprehension
-- [ output | generator, predicates]
-- [x^2 | x <- [1..10]]
-- [1,4,9,16,25,36,49,64,81,100]
-- [4,16,36,64,100]
-- [(1,64),(1,81),(1,100),(4,64),(4,81),(4,100) ...]
-- [(1,64),(1,81),(1,100),(4,64),(4,81)]
