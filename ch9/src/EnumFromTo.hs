module EnumFromTo
  (
  ) where

eftBool :: Bool -> Bool -> [Bool]
eftBool True True = [True]
eftBool False True = [False, True]
eftBool False False = [False]
eftBool True False = []

eftOrd :: Ordering -> Ordering -> [Ordering]
eftOrd LT LT = [LT]
eftOrd LT EQ = [LT, EQ]
eftOrd LT GT = [LT, EQ, GT]
eftOrd EQ EQ = [EQ, EQ]
eftOrd EQ GT = [EQ, GT]
eftOrd GT GT = [GT, GT]
eftOrd _ _ = []

eftInt :: Int -> Int -> [Int]
eftInt from to
  | from < to = []
  | from == to = [from]
  | from > to = from : eftInt (succ from) to

eftChar :: Char -> Char -> [Char]
eftChar from to
  | from < to = []
  | from == to = [from]
  | from > to = from : eftChar (succ from) to
