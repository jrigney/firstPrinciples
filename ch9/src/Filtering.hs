module Filtering
  (
  ) where

threeFilter :: [Integer] -> [Integer]
threeFilter = filter (\x -> mod x 3 == 0)

threeTimes :: [Integer] -> Int
threeTimes = length . threeFilter

myFilter :: String -> [String]
-- myFilter x =
--   let xs = words x
--   in filter (\x -> not $ elem x ["the", "a", "an"]) xs
myFilter = filter (\x -> not $ elem x ["the", "a", "an"]) . words
