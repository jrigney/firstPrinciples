# ch9

## Spines

weak head normal form (WHNF) - expression is only evaluated as far as is necessary to reach a data constructor or lambda waiting for an argument.

normal form (NF) - an expression with nothing further to reduce

expressions in WHNF may evaluate further once another argument is provided.  If further evaluation is not possible then the expression is in normal form (NF)

expression in NF is also WHNF

expression in WHNF might not be NF because there is more to be evaluated.

the difference between NF and WHNF seems to be that an NF expression can't reduce any more and WHNF will stop when it runs into a lambda or data constructor.

spine-strict functions can force a complete evaluation of the spine but not the values.

pattern matching is strict by default

length is spine-strict



