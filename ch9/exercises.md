
## Bottom Madness

### blow up
1. bottom
2. 1^2
3. bottom
4. 3
5. bottom
6. 2
7. bottom
8. 3
9. [1,3]
10. bottom

### normal form
1. neither
2. neither
3. WHNF
4. NF
5. WHNF
6. WHNF
7. NF

## More Bottoms
1. bottom
2. 2
3. bottom
4. returns [Bool] where true is vowel
5a. 1^2 2^2 3^2
5b. [1,10,20]
5c. [15,15,15]

