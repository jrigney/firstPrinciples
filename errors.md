### Partially applied function returned when a function result is expected

#### Code
```
data Optional a
  = Yep a
  | Nada

instance Foldable Optional where
  foldr f z (Yep x) = f x
```

#### Error message

```
    * Couldn't match expected type 'b' with actual type 'b -> b'
      'b' is a rigid type variable bound by
        the type signature for:
          foldr :: forall a b. (a -> b -> b) -> b -> Optional a -> b
        at /home/mark/fastProjects/firstPrinciples/ch20/src/Instances.hs:17:3-7
    * In the expression: f x
      In an equation for 'foldr': foldr f z (Yep x) = f x
      In the instance declaration for 'Foldable Optional'
    * Relevant bindings include
        z :: b
          (bound at /home/mark/fastProjects/firstPrinciples/ch20/src/Instances.hs:17:11)
        f :: a -> b -> b
          (bound at /home/mark/fastProjects/firstPrinciples/ch20/src/Instances.hs:17:9)
        foldr :: (a -> b -> b) -> b -> Optional a -> b
          (bound at /home/mark/fastProjects/firstPrinciples/ch20/src/Instances.hs:17:3)
   |
17 |   foldr f z (Yep x) = f x
   |                       ^^^
```

#### Explanation
So b here is the second parameter in the `foldr :: forall a b. (a -> b -> b) -> ` **b** ` -> Optional a -> b`
but b is also the second parameter in `(a -> ` **b** ` -> a)` which is the f in
 `foldr ` **f** ` z (Yep x) = `*f*` x`.  foldr returns a `b` but `f x` will return
 `b -> b` since it is partially applied with `x`.  The type foldr is expected to
 return is no the same as what is returned by `f x`

You need to get `f x` to return a `b` instead of `b -> b`.  By applying `f x z`
you will get back a b as defined by `(a -> b ->b)`


### Ambiguous type
#### Code
```
data Optional a
  = Yep a
  | Nada

instance Foldable Optional where
  foldMap f (Yep x) = f x
  foldMap f Nada = mempty
```

#### Error message
```
*Instances> foldMap (+1) Nada

    * Ambiguous type variable ‘a0’ arising from a use of ‘print’
      prevents the constraint ‘(Show a0)’ from being solved.
      Probable fix: use a type annotation to specify what ‘a0’ should be.
      These potential instances exist:
        instance (Show b, Show a) => Show (Either a b)
          -- Defined in ‘Data.Either’
        instance Show Ordering -- Defined in ‘GHC.Show’
        instance Show Integer -- Defined in ‘GHC.Show’
        ...plus 23 others
        ...plus 42 instances involving out-of-scope types
        (use -fprint-potential-instances to see them all)
    * In a stmt of an interactive GHCi command: print it
```

#### Explanation

It is attempting to "Show" the result of the fuction call but it does not know
what the return type is.  This is kind of a red herring.  Looking  at 
`foldMap f Nada = mempty` the return is mempty.  So you need to declare what
type of Monoid Nada is.

#### Corrected
```
import Data.Monoid
foldMap f Nada :: Sum Int
foldMap f Nada :: Product Int
```

### Missing function

#### Code
```
minimum' :: (Foldable t, Ord a) => t a -> Maybe a
```

#### Error message
```
    The type signature for ‘minimum'’ lacks an accompanying binding
   |
20 | minimum' :: (Foldable t, Ord a) => t a -> Maybe a
   | ^^^^^^^^
```

#### Explanation

`minimum' :: (Foldable t, Ord a) => t a -> Maybe a` is only a type definition.
You need a function definition.
```

### More complicated Ambiguous types
#### Code
```
displayAge maybeAge =
  case maybeAge of
    Nothing -> putStrLn "You provided an invalid year"
    Just age -> putStrLn $ "In 2020, you will be:" ++ show age

yearDiff futureYear birthYear = futureYear - birthYear

bday2VD = do
  putStrLn "Please enter you birth year"
  birthYearString &lt- getLine
  putStrLn "Enter some year in future"
  futureYearString &lt- getLine
  let maybeAge = do
        futureYear &lt- readMaybe futureYearString
        birthYear &lt- readMaybe birthYearString
        return $ yearDiff futureYear - birthYear
  displayAge maybeAge
```

#### Error message
```
/Users/jo/projects/appliedHaskell/mreview/src/FuncMon.hs:76:23: error:
    • Ambiguous type variable ‘a0’ arising from a use of ‘readMaybe’
      prevents the constraint ‘(Read a0)’ from being solved.
      Relevant bindings include
        maybeAge :: Maybe (a0 -> a0)
          (bound at /Users/jo/projects/appliedHaskell/mreview/src/FuncMon.hs:75:7)
      Probable fix: use a type annotation to specify what ‘a0’ should be.
      These potential instances exist:
        instance Read Ordering -- Defined in ‘GHC.Read’
        instance Read Integer -- Defined in ‘GHC.Read’
        instance Read a => Read (Maybe a) -- Defined in ‘GHC.Read’
        ...plus 22 others
        ...plus 7 instances involving out-of-scope types
        (use -fprint-potential-instances to see them all)
    • In a stmt of a 'do' block:
        futureYear &lt- readMaybe futureYearString
      In the expression:
        do futureYear &lt- readMaybe futureYearString
           birthYear &lt- readMaybe birthYearString
           return $ yearDiff futureYear - birthYear
      In an equation for ‘maybeAge’:
          maybeAge
            = do futureYear &lt- readMaybe futureYearString
                 birthYear &lt- readMaybe birthYearString
                 return $ yearDiff futureYear - birthYear
   |
76 |         futureYear &lt- readMaybe futureYearString
   |                       ^^^^^^^^^^^^^^^^^^^^^^^^^^

/Users/jo/projects/appliedHaskell/mreview/src/FuncMon.hs:77:22: error:
    • No instance for (Read (a0 -> a0))
        arising from a use of ‘readMaybe’
        (maybe you haven't applied a function to enough arguments?)
    • In a stmt of a 'do' block: birthYear &lt- readMaybe birthYearString
      In the expression:
        do futureYear &lt- readMaybe futureYearString
           birthYear &lt- readMaybe birthYearString
           return $ yearDiff futureYear - birthYear
      In an equation for ‘maybeAge’:
          maybeAge
            = do futureYear &lt- readMaybe futureYearString
                 birthYear &lt- readMaybe birthYearString
                 return $ yearDiff futureYear - birthYear
   |
77 |         birthYear &lt- readMaybe birthYearString
   |                      ^^^^^^^^^^^^^^^^^^^^^^^^^

/Users/jo/projects/appliedHaskell/mreview/src/FuncMon.hs:78:18: error:
    • Ambiguous type variable ‘a0’ arising from a use of ‘yearDiff’
      prevents the constraint ‘(Num a0)’ from being solved.
      Relevant bindings include
        birthYear :: a0 -> a0
          (bound at /Users/jo/projects/appliedHaskell/mreview/src/FuncMon.hs:77:9)
        futureYear :: a0
          (bound at /Users/jo/projects/appliedHaskell/mreview/src/FuncMon.hs:76:9)
        maybeAge :: Maybe (a0 -> a0)
          (bound at /Users/jo/projects/appliedHaskell/mreview/src/FuncMon.hs:75:7)
      Probable fix: use a type annotation to specify what ‘a0’ should be.
      These potential instances exist:
        instance Num Integer -- Defined in ‘GHC.Num’
        instance Num Double -- Defined in ‘GHC.Float’
        instance Num Float -- Defined in ‘GHC.Float’
        ...plus two others
        ...plus one instance involving out-of-scope types
        (use -fprint-potential-instances to see them all)
    • In the first argument of ‘(-)’, namely ‘yearDiff futureYear’
      In the second argument of ‘($)’, namely
        ‘yearDiff futureYear - birthYear’
      In a stmt of a 'do' block: return $ yearDiff futureYear - birthYear
   |
78 |         return $ yearDiff futureYear - birthYear
   |                  ^^^^^^^^^^^^^^^^^^^

/Users/jo/projects/appliedHaskell/mreview/src/FuncMon.hs:78:18: error:
    • No instance for (Num (a0 -> a0)) arising from a use of ‘-’
        (maybe you haven't applied a function to enough arguments?)
    • In the second argument of ‘($)’, namely
        ‘yearDiff futureYear - birthYear’
      In a stmt of a 'do' block: return $ yearDiff futureYear - birthYear
      In the expression:
        do futureYear &lt- readMaybe futureYearString
           birthYear &lt- readMaybe birthYearString
           return $ yearDiff futureYear - birthYear
   |
78 |         return $ yearDiff futureYear - birthYear
   |                  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

/Users/jo/projects/appliedHaskell/mreview/src/FuncMon.hs:79:3: error:
    • No instance for (Show (a0 -> a0))
        arising from a use of ‘displayAge’
        (maybe you haven't applied a function to enough arguments?)
    • In a stmt of a 'do' block: displayAge maybeAge
      In the expression:
        do putStrLn "Please enter you birth year"
           birthYearString &lt- getLine
           putStrLn "Enter some year in future"
           futureYearString &lt- getLine
           ....
      In an equation for ‘bday2VD’:
          bday2VD
            = do putStrLn "Please enter you birth year"
                 birthYearString &lt- getLine
                 putStrLn "Enter some year in future"
                 ....
   |
79 |   displayAge maybeAge
   |   ^^^^^^^^^^^^^^^^^^^

```

#### Explanation
There are four errors that are all related.

#### Corrected 
```
displayAge maybeAge =
  case maybeAge of
    Nothing -> putStrLn "You provided an invalid year"
    Just age -> putStrLn $ "In 2020, you will be:" ++ show age

yearDiff futureYear birthYear = futureYear - birthYear

bday2VD = do
  putStrLn "Please enter you birth year"
  birthYearString &lt- getLine
  putStrLn "Enter some year in future"
  futureYearString &lt- getLine
  let maybeAge = do
        futureYear &lt- readMaybe futureYearString
        birthYear &lt- readMaybe birthYearString
        return $ yearDiff futureYear birthYear
  displayAge maybeAge
```


