module Ch21Exercises
  (
  ) where

import Data.Monoid
import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

newtype Identity a =
  Identity a
  deriving (Eq, Ord, Show)

instance Functor Identity where
  fmap f (Identity x) = Identity $ f x

instance Applicative Identity where
  pure x = Identity x
  (Identity f) <*> x = fmap f x

instance Foldable Identity where
  foldMap f (Identity x) = f x

instance Traversable Identity where
  traverse f (Identity x) = Identity <$> (f x)

instance (Arbitrary a) => Arbitrary (Identity a) where
  arbitrary = do
    a <- arbitrary
    return $ Identity a

instance (Eq a) => EqProp (Identity a) where
  (=-=) = eq

testTraversableIdentity = quickBatch $ traversable (undefined :: Identity ([Int], [Int], [[Int]]))

newtype Constant a b = Constant
  { getConstant :: a
  } deriving (Eq, Show)

instance Functor (Constant a) where
  fmap _ (Constant a) = Constant a

instance (Monoid a) => Applicative (Constant a) where
  pure _ = Constant mempty
  (<*>) (Constant f) (Constant a) = Constant $ f <> a

instance Foldable (Constant a) where
  foldr _ z _ = z

-- traverse :: (Applicative f, Traversable t) => (a -> f b) -> t a -> f (t b)
-- remember a and b could be the same types so here a is the same as b
instance Traversable (Constant a) where
  traverse _ (Constant a) = pure $ Constant a

instance (Arbitrary a, Arbitrary b) => Arbitrary (Constant a b) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    elements [(Constant a), (Constant b)]

instance (Eq a) => EqProp (Constant a b) where
  (=-=) = eq

testTraversableConstant =
  quickBatch $ traversable (undefined :: Constant ([Int], [Int], [Int]) ([Int], [Int], [Int]))

data Optional a
  = Nada
  | Yep a
  deriving (Eq, Show)

instance Functor Optional where
  fmap _ (Nada) = Nada
  fmap f (Yep x) = Yep $ f x

instance Applicative Optional where
  pure x = Yep x
  (<*>) (Yep f) (Yep x) = Yep $ f x
  (<*>) _ (Nada) = Nada
  (<*>) (Nada) _ = Nada

instance Foldable (Optional) where
  foldr f z (Yep a) = f a z
  foldr _ z (Nada) = z

instance Traversable (Optional) where
  traverse f (Yep a) = Yep <$> f a
  traverse _ (Nada) = pure Nada

instance (Arbitrary a) => Arbitrary (Optional a) where
  arbitrary = do
    a <- arbitrary
    elements [Yep a, Nada]

instance (Eq a) => EqProp (Optional a) where
  (=-=) = eq

testApplicativeOptional = quickBatch $ applicative (undefined :: Optional (Int, Int, Int))

testTraversableOptional = quickBatch $ traversable (undefined :: Optional ([Int], [Int], [Int]))

data List a
  = Nil
  | Cons a
         (List a)
  deriving (Eq, Show)

instance Functor (List) where
  fmap _ Nil = Nil
  fmap f (Cons x xs) = Cons (f x) (fmap f xs)

instance Applicative (List) where
  pure x = Cons x Nil
  (<*>) _ Nil = Nil
  (<*>) (Cons f fs) (Cons x xs) = Cons (f x) (fs <*> xs)

instance Foldable (List) where
  foldr _ z Nil = z
  foldr f z (Cons x xs) = f x (foldr f z xs)

-- pattern of partially applying a function and lifting the function over the
-- structure
-- (<$>) :: Functor f => (a -> b) -> f a -> f b
-- traverse :: (Applicative f, Traversable t) => (a -> f b) -> t a -> f (t b)
instance Traversable (List) where
  traverse f Nil = pure Nil
  traverse f (Cons x xs) = Cons <$> (f x) <*> traverse f xs

instance (Arbitrary a) => Arbitrary (List a) where
  arbitrary = do
    a <- listOf arbitrary
    return $ (foldr (\x acc -> Cons x acc) Nil a)

instance (Eq a) => EqProp (List a) where
  (=-=) = eq

testTraversableList = quickBatch $ traversable (undefined :: List ([Int], [Int], [Int]))

data Three a b c =
  Three a
        b
        c

data Big a b =
  Big a
      b
      b
  deriving (Eq, Show)

instance Functor (Big a) where
  fmap f (Big x y1 y2) = Big x (f y1) (f y2)

instance (Monoid a) => Applicative (Big a) where
  pure x = Big mempty x x
  (<*>) (Big a b1 b2) (Big x y1 y2) = Big x (b1 y1) (b2 y2)

-- remember we are trying to smash the results together so use the accumulator z
instance Foldable (Big a) where
  foldr f z (Big x y1 y2) = f y1 newZ
    where
      newZ = f y2 z

-- Big x -> b -> b -> Big a b
-- Big x <$> f y1 :: f (b -> Big a b)
-- think about taking advantage of currying.  Big x will give you a function
-- b -> b -> Big a b so fmap the first b which will give you f (b -> Big a b)
-- you now have structure f so you need the tie fighter to lift the function
instance Traversable (Big a) where
  traverse f (Big x y1 y2) = (Big x <$> f y1) <*> f y2

instance (Arbitrary a, Arbitrary b) => Arbitrary (Big a b) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    return $ Big a b b

instance (Eq a, Eq b) => EqProp (Big a b) where
  (=-=) = eq

testTraversableBig =
  quickBatch $ traversable (undefined :: Big ([Int], [Int], [Int]) ([Int], [Int], [Int]))
