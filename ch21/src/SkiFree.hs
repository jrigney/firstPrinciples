{-# LANGUAGE FlexibleContexts #-}

module SkiFree
  (
  ) where

import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

-- n is a type constructor
data S n a =
  S (n a)
    a
  deriving (Eq, Show)

instance (Functor n, Arbitrary (n a), Arbitrary a) => Arbitrary (S n a) where
  arbitrary = S <$> arbitrary <*> arbitrary

instance (Applicative n, Testable (n Property), EqProp a) => EqProp (S n a) where
  (S x y) =-= (S p q) = (property $ (=-=) <$> x <*> p) .&. (y =-= q)

-- the given Arbitrary instance has n as a Functor
instance (Functor n) => Functor (S n) where
  fmap f (S n a) = S (fmap f n) (f a)

-- foldr returns a combined single value
-- I do not know what n is but if n can be folded the fold it
instance (Foldable n) => Foldable (S n) where
  foldr f z (S n a) = foldr f (f a z) n

-- think about currying the data construtor
-- you need to change n a to n b so traverse f n
instance Traversable n => Traversable (S n) where
  traverse f (S n a) = S <$> traverse f n <*> f a

main = sample' (arbitrary :: Gen (S [] Int))
