module Tree
  (
  ) where

import Control.Monad (liftM, liftM3)
import Data.Monoid ((<>))
import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

data Tree a
  = Empty
  | Leaf a
  | Node (Tree a)
         a
         (Tree a)
  deriving (Eq, Show)

instance Functor Tree where
  fmap _ Empty = Empty
  fmap f (Leaf a) = Leaf (f a)
  fmap f (Node l v r) = Node (fmap f l) (f v) (fmap f r)

-- foldMap is a bit easier
-- and looks more natural,
-- but you can do foldr too
-- for extra credit.
instance Foldable Tree where
  foldMap _ Empty = mempty
  foldMap f (Leaf a) = f a
  foldMap f (Node l v r) = foldMap f l <> f v <> foldMap f r

instance Traversable Tree where
  traverse _ Empty = pure Empty
  traverse f (Leaf x) = Leaf <$> f x
  traverse f (Node l v r) = Node <$> traverse f l <*> f v <*> traverse f r

-- Gen is a monad so liftM and liftM3 can be used
arbTree :: Arbitrary a => Int -> Gen (Tree a)
arbTree 0 = return Empty
arbTree n
  | n > 0 = oneof [liftM Leaf arbitrary, liftM3 Node subtree arbitrary subtree]
  where
    subtree = arbTree (n `div` 2)

instance (Arbitrary a) => Arbitrary (Tree a) where
  arbitrary = sized arbTree

instance (Eq a) => EqProp (Tree a) where
  (=-=) = eq

testTraversableTree = quickBatch $ traversable (undefined :: Tree ([Int], [Int], [[Int]]))
