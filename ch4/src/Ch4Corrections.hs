--Screen RC2 Pg. 111
module Ch4Corrections where

--1
x = (+)

f xs = w `x` 1
  where
    w = length xs

--2
id' = \x -> x --3

--3
g (a, b) = a
