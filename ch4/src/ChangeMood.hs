module ChangeMood
  (
  ) where

data Mood
  = Blah
  | Woot
  deriving (Eq, Show)

changeMood :: Mood -> Mood
changeMood Blah = Woot
changeMood Woot = Blah
